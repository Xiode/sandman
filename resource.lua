--Global resource handler. Loads and manages shared textures, sounds, etc
local Resource = {}

Resource.textures = {}
Resource.sounds = {}

--resource locations
local TEXTURES_FOLDER_TARGET = "textures/"
local SOUNDS_FOLDER_TARGET = "sounds/"	

--[[
potential problems:

- all resources are loaded at once!
- doesn't really allow for certain resources to be processed differently (ie. setting filtermode)

]]

local function loadSound(file)
	return love.audio.newSource(file, "static")
end

function Resource:load()
	print("loading textures...")
	self:loadResourceDirectory(TEXTURES_FOLDER_TARGET, self.textures, love.graphics.newImage)
	print("loading sounds...")
	self:loadResourceDirectory(SOUNDS_FOLDER_TARGET, self.sounds, loadSound)
end

--local directory, table to be loaded into, and the Love2D function for handling that filetype
function Resource:loadResourceDirectory(directory, resourcetable, func)
	
	print("loading directory " .. directory)

	files = love.filesystem.getDirectoryItems(love.filesystem.getIdentity() .. "/" .. directory)

	local subdirs = {} --located subdirectories

	for i=1,table.getn(files) do

		pos = (files[i]:len()-3) --Position of 3rd from last character, to check for 3-character file extentions to avoid trying to load folders as resources

		afiles = directory .. files[i] --appended file path

		if files[i]:sub(pos,pos) == "." then
			resourcetable[afiles] = assert(func(love.filesystem.getIdentity() .. '/' .. afiles), "Invalid file: " .. afiles)
			print("           loaded " .. afiles)
			if func == love.graphics.newImage then
				
			end
		else
			table.insert(subdirs, afiles)
		end
	end

	for i=1,table.getn(subdirs) do
		self:loadResourceDirectory(subdirs[i] .. "/", resourcetable, func) --lol recursion
	end

end

function Resource:getTexture(name)
	return self.textures[name] 
		or self.textures["textures/" .. name]
end

function Resource:getSound(name)
	return self.sounds[name] 
		or self.sounds["sounds/" .. name]
end

function Resource:playSound(name) --plays a sound from the beginning
	self:getSound(name):rewind()
	self:getSound(name):play()
end

return Resource