local Camera = CopyTable(Subject)

Camera.x = 0
Camera.y = 0

Camera.scalex = 1
Camera.scaley = 1

Camera.rotation = 0

function Camera:update(dt)
	cameraTarget = self:isEvent("cameraTarget") 
	if cameraTarget then
		self:target(cameraTarget.x, cameraTarget.y)
	end

	cameraMove = self:isEvent("cameraMove")
	if self:isEvent("cameraMove") then
		self:move(cameraMove.dx*dt, cameraMove.dy*dt)
	end

	self.events = {}
end

function Camera:set(d) --d = distance/depth, for parallax - 1 is default, 0 is infinity. ox,oy = origin
	if d == nil then d = 1 end

	hw = love.graphics.getWidth()*.5
	hh = love.graphics.getHeight()*.5

	love.graphics.push() --Push new state and apply transforms
	love.graphics.rotate(-self.rotation)
	love.graphics.scale(
		d/self.scalex,
		d/self.scaley
		)
	love.graphics.translate(
		( -self.x*d ),
		( -self.y*d )
		) --maybe this is how parallax works huehuehue, I am not of math
	
end

function Camera:unset()
	love.graphics.pop() --Remove state at end of draw loop
end

function Camera:move(dx, dy)
	self.x = self.x + (dx or 0)
	self.y = self.y + (dy or 0)
end

function Camera:scale(sx, sy)
	sx = sx or 1

	self.scalex = self.scalex * sx
	self.scaley = self.scaley * (sy or sx)
end

function Camera:setPosition(x,y)
	self.x = x or self.x
	self.y = y or self.y
end

function Camera:target(x, y)
	self.x = x	-(love.graphics.getWidth()/2)*self.scalex
	self.y = y	-(love.graphics.getHeight()/2)*self.scaley
end

function Camera:setScale(sx, sy)
	self.scalex = sx or self.scalex
	self.scaley = sy or self.scaley
end

return Camera