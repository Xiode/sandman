--CONSOLE = true
SHOW_LOAD_RESOURCES = true

if CONSOLE then
	require 'libs/lovedebug'
end

require 'util'

--libraries
yui = require 'libs/yaoui'
Input = require 'libs/boipushy'
bump = require 'libs/bump'

Resource = require 'resource'
Camera = require 'camera'  
UI = require 'ui'
World = require 'world'

dong = CopyTable(Subject)

Player = {}

function love.load()
	love.graphics.setDefaultFilter("linear", "nearest", 16)

	Resource:load()

	input = Input() --initialize input
	--[[
	input:bind('space',	'Jump')
	input:bind('w', 	'Jump')
	input:bind('a', 	'MoveLeft')
	input:bind('d', 	'MoveRight')
	input:bind('s', 	'Crouch')
	input:bind('lctrl', 'Crouch')
	input:bind('l', 	'AttackRight')
	input:bind('j', 	'AttackLeft')
	input:bind('i', 	'AttackUp')
	input:bind('return','Shield')
	input:bind('lshift','Sprint')
	input:bind('c',		'Dodge')

	input:bind('9', 'DevSaveWorld')
	input:bind('0', 'DevLoadWorld')
	]]
	input.binds = table.load( love.filesystem.getIdentity() .. "/controls")

	yui.UI.registerEvents()
	--yui.debug_draw = true
	UI:setTheme(yui)
	UI:build(yui)

	--Observers
	UI:addObserver(UI)
	UI:addObserver(dong)

	World:addObserver(UI)

	World:init()

	--[[	
	Player = World:addEntity(
		1, 
		200,330,2,
		"characters/sandman/sandman_32x32.png",Resource:getTexture("characters/sandman/sandman_32x32.png"),
		31,31,1,
		24,0,7,31)]]
	

	Player = World:addEntity(
		1, --layer
		200,300, --x,y pos
		"entities/player") --entity definition file
	Player:loopAnim("idle")

	World:setFilter(Player, "playerfilter")

	World:addBrush(1, 50,400,15000,100, "wall", "garbage.png")
	World:addBrush(1, 50,200,100,200, "wall", "garbage.png")
	World:addBrush(1, 1450,400-64,500,32, "wall", "garbage.png")

	World:addBrush(2, 1450,400-64,500,32, "noclip", "garbage.png")
	World:setDepth(2, .9)

	World:addBrush(1, 1450,400-64,500,32, "noclip", "garbage.png")
end

camtgt = {x=0,y=0}
cammod = {x=0,y=0}
Camera:scale(.80)

function love.update( dt )
	Camera:update(dt)

	if input:down("LookUp") then
		cammod.y = -200
	elseif input:down("LookDown") then
	    cammod.y = 	200
	else
		cammod.y = 0
	end

	camtgt.x = timelerp(camtgt.x, Player.x+(Player.velocityx*.8)+cammod.x, .05, dt)
	camtgt.y = timelerp(camtgt.y, Player.y+cammod.y, .05, dt)

	Camera:target(camtgt.x, camtgt.y)
	World:update(dt)

	yui.update({})
	UI:update(dt)
end

function love.draw()
	--Camera:set()
	World:draw()
	--Camera:unset()
	UI:draw()
end

--[[

function love.errhand( msg ) end

function love.threaderror(thread, errorstr) end

function love.filedropped( file )
end

function love.focus( focus )
end

function love.resize( w, h )
end

function love.keypressed(key)
end

function love.keyreleased(key)
end


function love.mousefocus( focus )
end

function love.mousemoved( x, y, dx, dy, istouch )
end

function love.mousepressed( x, y, button, istouch )
end

function love.mousereleased( x, y, button, istouch )
end

function love.textinput( text )
end

function love.wheelmoved(x, y)
end

function love.visible( visible )
end

function love.quit()
end

]]