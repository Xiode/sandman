--Options tabs--
--Allows for navigation between options menus.

return yui.View(love.graphics.getWidth()/3,love.graphics.getHeight()/3,love.graphics.getWidth()/3,50, {
	yui.Flow({spacing = 5,

		yui.Tabs({tabs = {
			{name = 'controls',  text = 'Controls', onClick = function(self) UI:notify("optiontabs_controls") end},
			{name = 'video', text = 'Video', onClick = function(self) UI:notify("optiontabs_video") end},
			{name = 'audio', text = 'Audio', onClick = function(self) UI:notify("optiontabs_audio") end}}
			})
		
	})
})