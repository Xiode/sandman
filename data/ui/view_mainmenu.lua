--Main Menu--
--Allows access to options menu, 

return yui.View(love.graphics.getHeight()*.1,love.graphics.getHeight()*.1,400,love.graphics.getHeight()-(love.graphics.getHeight()*.2), {
	margin_top = 10,
	margin_left = 10,
	
	yui.Stack({margin_top = 10, margin_left = 10, margin_right = 10, margin_bottom = 10, spacing = 5,

		name = 'MainFlow',

		yui.Button({name = 'newgame',  text = 'NEW GAME', onClick = function(self) UI:notify("ui_newgame") end}),
		yui.Button({name = 'loadgame', text = 'LOAD GAME', onClick = function(self) UI:notify("ui_loadgame") end}),
		yui.Button({name = 'options', text = 'OPTIONS', onClick = function(self) UI:notify("ui_options") end}),

		bottom = {
			yui.Button({text = 'EXIT', hover = 'EXIT TO DESKTOP', onClick = function(self) UI:notify("ui_quit") end})
		}
	})
})