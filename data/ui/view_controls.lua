--Controls--
--Key bindings, gamepad setup, etc.

--to match produced labels to actual binds in table
local bindOptions = {}

local function populateOptions()

	o = {}
	ibinds = {}

	for a in pairs(input.binds) do
		table.insert(ibinds, a)
	end
	table.sort(ibinds)

--	for action,bindtable in ipairs(ibinds) do
	for i=1,#ibinds do
		action		= ibinds[i]
		bindtable 	= input.binds[ibinds[i]]

		binds = ""

		for i=1,#bindtable do
			binds = binds .. bindtable[i]

			if i<#bindtable then
				--for all but the last bind, add a comma and space
				binds = binds .. ', '
			end
		end

		label = action .. ' (' .. binds .. ')'

		table.insert(o, label)
		bindOptions[label] = action
	end

	return o
end

return yui.View(love.graphics.getWidth()/3,love.graphics.getHeight()/3+50,love.graphics.getWidth()/3,love.graphics.getHeight()/3, {
	yui.Stack({spacing = 5,

		yui.Flow({
			yui.Dropdown({options = populateOptions(),
				onSelect = function(self, option) UI:notify("") --[[print(option, bindOptions[option])]]  end})
		})

	})
})