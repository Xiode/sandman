--Controls--
--Key bindings, gamepad setup, etc.

--to match produced labels to actual binds in table
local files = love.filesystem.getDirectoryItems("saves/")
local fileOptions = {}


local function populateOptions()
	o = {}

	table.insert(o, "Load game...")

	for i=1,#files do
		if files[i] ~= "data" then --don't list the data folder
			label = files[i]

			table.insert(o, label)
			fileOptions[label] = files[i]
		end
	end

	return o
end

return yui.View(love.graphics.getWidth()/3,love.graphics.getHeight()/3+50,love.graphics.getWidth()/3,love.graphics.getHeight()/3, {
	yui.Stack({spacing = 5,

		yui.Flow({
			yui.Dropdown({options = populateOptions(),
				onSelect = function(self, option) fileOptions.selected = fileOptions[option] end}),

			right = { yui.Button({text = "Resume", onClick = function(self) UI:notify("loadgame", fileOptions.selected) end}) }
		})

	})
})