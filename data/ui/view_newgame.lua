--New Game--

local difficulty = 'Normal'

return yui.View(love.graphics.getWidth()/3,love.graphics.getHeight()/3+50,love.graphics.getWidth()/3,love.graphics.getHeight()/3, {
	yui.Stack({spacing = 5,

		yui.Flow({
			yui.Dropdown({options = {'Normal', 'Hard', 'Ironman'},
				title = 'Difficulty: ',
				onSelect = function(self, option) difficulty = option end}),
			yui.FlatTextinput({name="playername"--[[, onEnter = function(self, text) if text ~= "" then UI:notify("new game", {text, difficulty}) end end]]}),

			right = {
				yui.Button({text = 'Begin', onClick = 
					function(self) 
						local pname = UI.views["newgame"][1][1].playername.textarea.text.text --holy fucking shit let me tell you about table hierarchies lmao
						if pname ~= "" then
							UI:notify("startnewgame", {pname, difficulty})
						end
					end})
			}
		})

	})
})