--Controls--
--Key bindings, gamepad setup, etc.

--to match produced labels to actual binds in table

--name (in ui), x res, y res
local resOptions = {
["Native / Auto"] = {0, 0},
["Custom"]	= {800,  600},
["4k"] 		= {3840, 2160},
["1080"]	= {1920, 1080},
["720"]		= {1280, 720}
}

local function populateOptions()
	local o = {}

	for k,v in pairs(resOptions) do
		table.insert(o,k)
	end

	resOptions.customX = 800
	resOptions.customY = 600
	resOptions.Custom = {resOptions.customX, resOptions.customY}

	return o
end

local customRes = {["x"]=800,["y"]=600}

return yui.View(love.graphics.getWidth()/3,love.graphics.getHeight()/3+50,love.graphics.getWidth()/3,love.graphics.getHeight()/3, {
	yui.Stack({spacing = 5,

		yui.Flow({
			yui.Dropdown({options = {"TODO"},
				onSelect = function(self, option) UI:notify("") --[[self[1][3]:setText("asdffdsa")]] end})
			--[[yui.Textinput({name = "customx", onEnter = function(self,text) resOptions.customX = tonumber(text) end}),
			yui.Textinput({name = "customy", onEnter = function(self,text) resOptions.customY = tonumber(text) end})]]
		})

	})
})