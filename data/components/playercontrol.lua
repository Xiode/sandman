local movespeed = 300
local sprint = 1.2
local sprinting = false

local accel = 0 --left/right movement acceleration counter. -1 to 1 (avoid modifying)
local acceleration = .999
local deceleration = .999

local stepped = false
local stepframes = {3, 8}
--[[
local stepsounds = {"sandman/sandstep1.ogg",
					"sandman/sandstep2.ogg",
					"sandman/sandstep3.ogg" }]]
local stepsounds = {"footsteps_sand1.wav",
					"footsteps_sand2.wav",
					"footsteps_sand3.wav",
					"footsteps_sand4.wav",
					"footsteps_sand5.wav",
					"footsteps_sand6.wav" }
	stepsounds.next = 1

acceleration = 1-acceleration
deceleration = 1-deceleration

return function(self,sim,dt)
	if self.grounded then --ground behaviour

		if self.velocityy > 0 then --if falling, but touching the ground...
			if self.velocityy > 100 then --we're falling fast enough to trigger a landing noise.
		 		Resource:playSound(stepsounds[3])
			end

			self.velocityy = 0 --stop falling
		end
		
		if input:down('Sprint') then sprinting = true else sprinting = false end

		--Force 1 unit over, to avoid getting stuck in geometry.
		if input:pressed('MoveRight') then
			self.goalx = self.x + 1
			--self.forcePosition = true
		elseif input:pressed('MoveLeft') then
			self.goalx = self.x - 1
			--self.forcePosition = true
		end

		--Move right
			if input:down('MoveRight') and not self.collideright then
				self.flip = false
				accel = timelerp(accel, sprinting and sprint or 1, acceleration, dt)
				self:loopAnim('run')
			elseif input:down('MoveLeft') and not self.collideleft then
				self.flip = true
				accel = timelerp(accel, sprinting and -sprint or -1, acceleration, dt)
				self:loopAnim('run')
			else
				accel = timelerp(accel, 0, deceleration, dt)
			end

		if math.abs(accel) > .2 then --if we're going above 20% speed...
			self:setAnimSpeed('run', (math.abs(accel^2))*20) --match the animation's play speed to velocity
			--wew lad. this is a bit gross.
			if self:currentFrame() == stepframes[1] or self:currentFrame() == stepframes[2] then 
				if stepped == false then --if we haven't stepped yet...
					Resource:playSound(stepsounds[stepsounds.next]) --play step sound
					stepsounds.next = stepsounds[stepsounds.next+love.math.random(1,3)] and stepsounds.next+1 or 1 --increment between 1 and 3 spots in the footsteps array, and if we overshoot, reset to 1
					stepped = true --we have successfully stepped.
				end
			else
				stepped = false --if we're not on a 'step' frame, reset
			end
		else
			self:loopAnim('idle')
		end

		if input:pressed('Jump') then
			
			Resource:playSound(stepsounds[2])
			
			self:loopAnim('fall')
			self:interject('jump')
			
			self.velocityy = -400

			self.goaly = self.y-1
			self.goalx = self.x
			self.forcePosition = true --force position for one frame to get out of any other objects
		end
	else --airborne behaviour
		self:loopAnim('fall')
		if self.collideright or self.collideleft then
			accel = 0
		end
	end

	self.velocityx = accel*movespeed*(sprinting and sprint or 1)
end


--[[
--old playercontrol--

local movespeed = 300
local accel = 0
local acceltime = .1 --time, in seconds, that it takes to reach full speed
local decelerate = 15
local sprint = 1.5
local sprinting = false

return function(self,sim,dt)
	if self.forcePosition then self.forcePosition = false end

	if input:pressed('MoveRight') then
		accel = 0
		self.goalx = self.x + 1
		self.forcePosition = true
	elseif input:pressed('MoveLeft') then
		accel = 0
		self.goalx = self.x - 1
		self.forcePosition = true
	end

	if self.grounded and not input:down('MoveRight') and not input:down('MoveLeft') then
		self:loopAnim("idle")
		if self.velocityy < 0 then
		end
		accel = accel-dt
		if accel < 0 then accel = 0 end
	end

	if input:down('Sprint') then sprinting = true else sprinting = false end

	if input:down('MoveRight') and self.grounded then
		if sprinting then self:loopAnim("sprint", true) else self:loopAnim("run", true) end
		accel = accel + dt
		self.flip = false
		if accel > acceltime then accel = acceltime end
		self.velocityx = (accel/acceltime)*movespeed*(sprinting and sprint or 1)
	elseif input:down('MoveLeft') and self.grounded then
		if sprinting then self:loopAnim("sprint") else self:loopAnim("run") end
		accel = accel + dt
		if accel > acceltime then accel = acceltime end
		self.flip = true
		self.velocityx = (accel/acceltime)*-movespeed*(sprinting and sprint or 1)
	elseif self.numCollisions > 0 then
		--self.velocityx = self.velocityx*(accel/acceltime)
		self.velocityx = lerp(self.velocityx, 0, dt*decelerate) --decelerate (ie. approach 0)
	else
		self:loopAnim("fall", false)
	end

	if self.grounded then --if I am grounded, then...
		if self.velocityy > 0 then self.velocityy = 0 end --cancel falling
		if input:pressed('Jump') then
			self:interject("jump")
			self.velocityy = -400
			self.goaly = self.y-1
			self.goalx = self.x
			self.forcePosition = true --force position for one frame to get out of any other objects
		end
	end

end]]