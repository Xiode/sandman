--base physics function. does gravity and movement based on velocity
return function(self,sim,dt)
	self.forceposition = false --order matters! add this file to an entity first
	self.velocityy = self.velocityy + (dt*self.gravity)

	self.goalx = self.goalx + (dt*self.velocityx)
	self.goaly = self.goaly + (dt*self.velocityy)

	self.grounded = false
	self.collideright = false
	self.collideleft = false
	self.collideup = false

	for i=1,self.numCollisions do
		if self.collisions[i].type == "slide" then
			if self.collisions[i].normal.x == 0 and
				self.collisions[i].normal.y == -1 then
				self.grounded = true
			end
			if self.collisions[i].normal.x == -1 and
				self.collisions[i].normal.y == 0 then
				self.collideright = true
			end
			if self.collisions[i].normal.x == 1 and
				self.collisions[i].normal.y == 0 then
				self.collideleft = true
			end
			if self.collisions[i].normal.x == 0 and
				self.collisions[i].normal.y == 1 then
				self.collideup = true
			end
		end

		if self.collideleft and self.velocityx < 0 then self.velocityx = 0 end
		if self.collideright and self.velocityx > 0 then self.velocityx = 0 end
	end
end