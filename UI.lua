local UI = CopyTable(Subject)

UI.views = {}
UI.viewsActive = {}

function UI:build(yui)
	--main menu
	self:addView("mainmenu", true, require ( love.filesystem.getIdentity() .. '/ui/view_mainmenu'))

	--options
	--serialize this shit?
	self:addView("options", false,				require ( love.filesystem.getIdentity() .. '/ui/view_optionstabs'))
	self:addView("options_controls", false,		require ( love.filesystem.getIdentity() .. '/ui/view_controls'))
	self:addView("options_video", false,		require ( love.filesystem.getIdentity() .. '/ui/view_video'))
	self:addView("options_audio", false,		require ( love.filesystem.getIdentity() .. '/ui/view_audio'))

	self:bindEvent("ui_quit", function()
		love.event.quit()
	end)

	self:bindEvent("ui_newgame", function()
		if self:isViewActive("newgame") then
			self:soloView("mainmenu")
		else
			self:soloView({"mainmenu", "newgame"})
		end
	end)

	self:bindEvent("ui_loadgame", function()
		if self:isViewActive("loadgame") then
			self:soloView("mainmenu")
		else
			self:soloView({"mainmenu", "loadgame"})
		end
	end)

	self:bindEvent("ui_options", function()
		if self:isViewActive("options") then
			self:soloView("mainmenu")
		else
			self:soloView({"mainmenu", "options", "options_controls"})
		end
	end)

	self:bindEvent("optiontabs_controls", function() 
		self:soloView({"mainmenu", "options", "options_controls"})
	end)
	self:bindEvent("optiontabs_video", function() 
		self:soloView({"mainmenu", "options", "options_video"})
	end)
	self:bindEvent("optiontabs_audio", function() 
		self:soloView({"mainmenu", "options", "options_audio"})
	end)

	--new game menu
	self:addView("newgame", false,	require ( love.filesystem.getIdentity() .. '/ui/view_newgame'))

	--load game menu
	self:addView("loadgame", false,	require ( love.filesystem.getIdentity() .. '/ui/view_loadgame'))
end

function UI:update(dt)
	for viewname,active in pairs(self.viewsActive) do
		if active then
			self.views[viewname]:update(dt)
		end
	end

	self.events = {} --flush events
end

function UI:draw()
	for viewname,active in pairs(self.viewsActive) do
		if active == true or self.viewsActive[active] == true then
			self.views[viewname]:draw()
		end
	end
end

function UI:addView(name, active, view)
	self.views[name] = view
	self.viewsActive[name] = active
end

function UI:toggleView(name)
	self.viewsActive[name] = not self.viewsActive[name]
end

function UI:enableView(name)
	if self.viewsActive[name] == nil then error("view \"" .. name .."\" does not exist!") end
	self.viewsActive[name] = true
end

function UI:disableView(name)
	self.viewsActive[name] = false --ditto
end

function UI:isViewActive(name)
	return self.viewsActive[name]
end

--takes a view name or a table
function UI:soloView(name)

	--disable all
	for k,v in pairs(self.viewsActive) do
		self:disableView(k)
	end

	--enable whatever is solo'd
	if type(name) == "table" then
		for i=1,#name do
			self:enableView(name[i])
		end
	else
		self:enableView(name)
	end
end

function UI:setTheme(yui)
	yui.Theme.colors["button_primary"] = {30,30,30}
	yui.Theme.colors["flat_textinput_selected_bg"] = {1,1,1}
end

return UI