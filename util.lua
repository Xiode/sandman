require 'serializetable'

Subject = {}
Subject.observers = {}
Subject.events = {}
Subject.eventBinds = {}

function Subject:addObserver(object)
    table.insert(self.observers, object)
end

function Subject:notify(selector, data) 
    if selector ~= nil then
        for o=1,#self.observers do
            self.observers[o]:receive(selector, data)
        end
    end
end

function Subject:receive(selector, data)
    table.insert(self.events, {selector,data}) --insert into event table
    if self.eventBinds[selector] then
        self.eventBinds[selector](data)
    end

end

function Subject:bindEvent(selector, func)
    self.eventBinds[selector] = func
end

function Subject:isEvent(selector)
    for k, v in pairs(self.events) do
        if v[1] == selector then
            return v[2] or true
        end
    end
    return false
end


--console print. prints to game console as well as Love2D's C console.
function cprint(...)
    --do console stuff
    print(...)
end

--table copy script from some luausers page
function CopyTable(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return setmetatable(new_table, getmetatable(object))
    end
    return _copy(object)
end

function lerp(pos1, pos2, perc)
    return (1-perc)*pos1 + perc*pos2 -- Linear Interpolation
end

--mathematically sound way to lerp with dt
--equivalent to lerp(pos1,pos2,1-perc^dt) 
function timelerp(pos1, pos2, perc, dt, time)
    return lerp(pos1,pos2,1-math.pow(perc,dt*(time or 1)))
end

--http://lua-users.org/wiki/SimpleRound
--rounds a float to roundto decimal places.
function math.round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

--[[
--obsolete?
function AABB(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

--2 objects positions and sizes, individually or as tables
--obsolete?
function SlopePosition(x,y,w,h, rx,ry,rw,rh)
    x,y,w,h, rx,ry,rw,rh = ParseObjectsBounds(x,y,w,h, rx,ry,rw,rh)

    return x, rh/(x-rx/rw)
end

--obsolete?
function ParseObjectsBounds(x1,y1,w1,h1, x2,y2,w2,h2) --Parses inputs of 2 object's positions and sizes, allowing for tables
    if y1.x then --If both objects are tables, extract the elements
        y2 = y1.y
        w2 = y1.w
        h2 = y1.h
        x2 = y1.x

        y = x.y
        w = x.w
        h = x.h
        x = x.x
    elseif x2 and x2.x then --If object 1s args were passed individually and the 2nd object is a table
        y2 = x2.y
        w2 = x2.w
        h2 = x2.h
        x2 = x2.x
    else -- if a player table and individual rectangle args were passed
        y = x.y
        w = x.w
        h = x.h
        x = x.x
    end --Otherwise, no parsing needed

    return x1,y1,w1,h1, x2,y2,w2,h2
end

]]