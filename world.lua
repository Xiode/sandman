local Entity = require 'entity'

local World = CopyTable(Subject)
World.currentWorld = {} --Holds the active positions of brushes, entities, and other data for the current gameworld.
World.spawns = {} --Holds spawn positions for everything. This is what gets saved/loaded
World.components = {}

World.filters = {}
World.filters.playerfilter = function(item,other)
	if 		other.collide == "none" then return
	elseif 	other.collide == "wall" then return 'slide'
	elseif 	other.isEnemy and item.dodge then return 'cross'
	end
end

World.filters.enemyfilter = function(item,other)
	if other.isPlayer and not other.dodge then return 'slide'
	end
end

--Initialize all the world tables
function World:init()
	local w = {}

	--[[layers: layers are treated equally, and are drawn as follows:
		brushes (bottom) -> 
		decals ->
		entities

		so, entities on layer 1 will draw over decals and brushes on layer 1, but under decals and brushes on layer 2 or above.
	]]

	w.brushes 		= {} 
	w.decals 		= {} 
	w.entities 		= {}

	self.spawns = w -- initialize the spawns table; layerless

	w.brushes[1]	= {} --default world layer
	w.decals[1] 	= {} --default decal layer
	w.entities[1] 	= {} --default entity layer
	
	self.currentWorld = w

	World:loadComponents(love.filesystem.getIdentity() .. "/components.ini")

	self.sim = bump.newWorld() --make a new world
	w = nil
end

--save the current spawn table to file
function World:save(file)
	table.save(self.spawns, file)
end

--load a spawn table from file
function World:load(file)
	self.spawns = table.load(file) --unsafe?
	self:spawn() --spawn everything into currentWorld
end

function World:spawn()
	for b=1,#self.spawns.brushes do
	end
	for b=1,#self.spawns.decals do
		--TODO
	end
	for b=1,#self.spawns.entities do
	end
end

function World:loadComponents(f)
	local c = table.load(f)
	for name,path in pairs(c) do
		assert(World:defineComponent(name, require(love.filesystem.getIdentity() .. '/' .. path)))
	end
	--World:defineComponent("basephysics", require 'components/basephysics')
	--World:defineComponent("playercontrol", require 'components/playercontrol')
end

function World:update(dt)
	if input:pressed('DevSaveWorld') then
		self:save('devworld')
	elseif input:pressed('DevLoadWorld') then
		self:load('devworld')
	end

	w = self.currentWorld

	for i=1,#w.entities do
		for e=1,#w.entities[i] do
			self:updateEntity(dt, w.entities[i][e])
		end
	end
end


function World:draw()
	w = self.currentWorld
	iters = math.max(#w.brushes, #w.decals, #w.entities) --iterate according to the largest table. could probably avoid doing this per frame, but whatever


	for i=1,iters do --draw by layer.
		if w.brushes[i] then
			for b=1,#w.brushes[i] do
				Camera:set(w.brushes[i].depth or 1, w.brushes[i].originx,w.brushes[i].originy)
				World:drawBrush(w.brushes[i][b])
				Camera:unset()
			end
		end
		if w.decals[i] then
			for d=1,#w.decals[i] do
				Camera:set(w.decals[i].depth or 1)
				World:drawDecal(w.decals[i][d])
				Camera:unset()
			end
		end
		if w.entities[i] then
			for e=1,#w.entities[i] do
				Camera:set(w.entities[i].depth or 1)
				World:drawEntity(w.entities[i][e])
				Camera:unset()
			end
		end
	end
end

function World:setDepth(layer, depth, originx, originy)
	if self.currentWorld.brushes[layer] then
		self.currentWorld.brushes[layer].depth = depth
		self.currentWorld.brushes[layer].originx = originx or self.currentWorld.brushes[layer].originx
		self.currentWorld.brushes[layer].originy = originy or self.currentWorld.brushes[layer].originy
	end
	if self.currentWorld.decals[layer] then self.currentWorld.decals[layer].depth = depth end
	if self.currentWorld.entities[layer] then self.currentWorld.entities[layer].depth = depth end
end

function World:updateEntity(dt, entity)
	entity.x, entity.y, entity.collisions, entity.numCollisions = self.sim:move(entity, entity.goalx,entity.goaly, entity.filter)
	entity:update(self.sim,dt)
end

function World:drawBrush(brush)
	love.graphics.draw(Resource:getTexture(brush.tex), brush.quad, brush.x, brush.y)
end

function World:drawDecal(decal)
	--TODO
end

function World:drawEntity(entity)
	if entity.flip then
		love.graphics.draw(Resource:getTexture(entity.tex), entity:getFrameQuad(), 
			math.floor(entity.x+((entity.spritesizex)*entity.s)-entity.bx),
			math.floor(entity.y-entity.by),
			0, -entity.s, entity.s)
	else
		love.graphics.draw(Resource:getTexture(entity.tex), entity:getFrameQuad(), 
			entity.x-entity.bx,
			entity.y-entity.by,
			0, entity.s, entity.s)
	end
end

--Add a brush, record it in the spawn table
function World:addBrush(layer, x,y,w,h, collide, tex)
	b = World:initBrush(layer,x,y,w,h,collide,tex)

	local bclean = CopyTable(b)

	bclean.quad = nil --don't include quad object in the spawnfile
	table.insert(self.spawns.brushes, bclean)
end

--Add a brush, do not record it in the spawn table
--World layer(number), xywh(numbers), collide(bool), tex(str)
function World:initBrush(layer, x,y,w,h, collide, tex)
	if self.currentWorld.brushes[layer] == nil then
		self.currentWorld.brushes[layer] = {}
	end

	b={}
	b.collide = collide
	b.layer = layer
	b.x=x
	b.y=y
	b.w=w
	b.h=h
	b.tex=tex

	Resource:getTexture(tex):setWrap("repeat")
	b.quad = love.graphics.newQuad(x,y,w,h,Resource:getTexture(tex):getWidth(),Resource:getTexture(tex):getHeight())


	table.insert(self.currentWorld.brushes[layer], b)
	if collide ~= "noclip" and collide ~= nil then
		self.sim:add(b,x,y,w,h)
	end

	return b, #self.currentWorld.brushes[layer]
end

--Record the entity in the spawn table, prepare it for
function World:addEntity(layer, x,y, file)
	local entity = {}
	entity.layer 	= layer
	entity.x 		= x
	entity.y 		= y
	entity.file 	= file
	table.insert(self.spawns, entity)

	return self:initEntity(layer, x,y, file)
end

--Initialize the entity into the world.
function World:initEntity(layer, x,y, file)
	if self.currentWorld.entities[layer] == nil then self.currentWorld.entities[layer] = {} end

	--local ef = table.load(love.filesystem.getIdentity() .. '/' .. file) --entity file
	local ef = dofile(love.filesystem.getIdentity() .. '/' .. file)

	
	if ef == nil then error("File \"" .. file .. "\" could not be read.") end

	e = Entity:new(
		x,y,
		ef.scale,
		ef.texture,
		Resource:getTexture(ef.texture),
		ef.spritesizex,
		ef.spritesizey,
		ef.spritegap,
		ef.boundsx,
		ef.boundsy,
		ef.boundsw,
		ef.boundsh)

	for a=1, #ef.anims do
		e:newAnimation(
			ef.anims[a][1],
			ef.anims[a][2], 
			ef.anims[a][3], 
			ef.anims[a][4])
	end

	for i=1, #ef.components do
		e:addComponent(self.components[ef.components[i]])
	end
	table.insert(self.currentWorld.entities[layer], e)
	self.sim:add(e, e.x+(e.bx*e.s),e.y+(e.by*e.s),e.bw*e.s,e.bh*e.s)

	return e
end

function World:defineComponent(name, func)
	if func then
		self.components[name] = func
		return true --succeeded!
	else
		print("Error: Couldn't load component \"" .. name .. "\"!")
		return false --failed!
	end
end

--add a component to an entity.
function World:addComponent(entity, func)
	if type(func) == "function" then entity:addComponent(func) return end
	if type(func) == "string" then entity:addComponent(self.components[func]) return end
	error("\"" .. tostring(func) .. "\" is not a valid component!")
end

--add a filter to an entity
function World:setFilter(entity, filtername)
	entity:setFilter(self.filters[filtername])
end

return World