local Entity = {}

function Entity:new(...)
	e = CopyTable(self)

	e:init(...)

	return e
end

--x,y,s(numbers) - x,y pos and scale, tex(string) - texture name and dir,
--spritesizex,y(numbers) - size of each frame in atlas, spritegap(number) - size of gap on the bottom and right side of each frame in atlas,
--boundsx,boundsy,boundsw,boundsh(numbers) - bounding box for Bump.lua, relative to x,y position
function Entity:init(x,y,s,tex,texData,spritesizex,spritesizey,spritegap,boundsx,boundsy,boundsw,boundsh)
	self.x=x
	self.y=y
	self.s=s

	self.bx = boundsx
	self.by = boundsy
	self.bw = boundsw
	self.bh = boundsh

	self.goalx = x
	self.goaly = y

	self.velocityx = 0
	self.velocityy = 0

	self.flip = false

	self.gravity = 1200 -- :v

	self.tex=tex

	self.texw = texData:getWidth()
	self.texh = texData:getHeight()

	self.spritesizex=spritesizex
	self.spritesizey=spritesizey
	self.spritegap=spritegap

	self.forcePosition = false
	self.grounded = false

	self:defineAtlas()

	self.curquad = 1
	self.curanim = 0
	self.loopanim = 0
	self.ac = 0 --animation counter

	self.animations = {}

	self.components = {}
end

function Entity:defineAtlas()
	--define atlas quads
	self.atlas = {}

	local ssx = self.spritesizex+self.spritegap
	local ssy = self.spritesizey+self.spritegap

	for ay=0,(self.texw/ssy)-1 do
		for ax=0,(self.texw/ssx)-1 do
			q = love.graphics.newQuad(
				ax*(ssx),
				ay*(ssy),
				self.spritesizex, self.spritesizey,
				self.texw, self.texh)
			table.insert(self.atlas, q)
		end
	end
end

function Entity:addComponent(c)
	table.insert(self.components, c)
end

function Entity:currentFrame()
	return self.curquad
end

function Entity:getFrameQuad(num)
	if num ~= nil and self.atlas[num] ~= nil then self.curquad = num end
	return self.atlas[self.curquad]
end

--1: name(string), start(number), finish(number), speed(number) - start and finish frames in texture atlas, as well as the playback speed in FPS.
--2: name(string), start(table), finish(number) - 'start' is a table of frame numbers, like so:
--   {1, 2, 3, 5, 6, 10, 11}
--   'finish' is the speed of the animation.
function Entity:newAnimation(name,start,finish,speed)
	a = {}
	a.name = name
	if type(start) == "table" then
		a.frames = start
		a.speed = finish
		a.lastframe = start[#start]
	else
		a.frames = {}
		for f=start,finish do
			table.insert(a.frames,f)
		end
		if name == "sprint" then
		end
		a.lastframe = finish
		a.speed = speed
	end

	self.animations[name] = a
end

function Entity:playAnim(name)
	if self.animations[name] == nil then error("\"" .. name .. "\" is not an animation!") end
	a = self.animations[name]
	if self.curanim == a then return a end
	self.curquad = a.frames[1]
	self.curanim = a
	return a
end

function Entity:interject(name)
	a = self:playAnim(name)
	self.curquad = a.frames[1]
end

function Entity:setAnimSpeed(name, speed)
	self.animations[name].speed = speed
end

function Entity:loopAnim(name, interrupt)
	if interrupt == nil and self.loopanim == self.curanim then
		self:playAnim(name) --interrupt if we're in a loop, but not in an interjection
	elseif interrupt then
	    self:playAnim(name) --explicit interrupt
	end
	self.loopanim = self.animations[name]
end

function Entity:update(sim,dt)
	for c=1,#self.components do
		self.components[c](self,sim,dt)
	end

	--anim loops
	if self.curanim ~= 0 then
		if self.ac > (1/self.curanim.speed) then --change frame
			--print(n,self.curquad)
			n=self.curquad+1 --next frame
			if n == nil or n > self.curanim.lastframe then --reached end of anim
				if self.loopanim and self.curanim ~= self.loopanim then
					self.curanim = self.loopanim --return to loopanim, if there is one and it is different from current anim
				end
				n = self.curanim.frames[1]
			end
			self.curquad = n
			self.ac = 0
		end
		self.ac = self.ac + dt
	end
end

function Entity:position(x,y)
	self.x = x or self.x
	self.y = y or self.y
end

function Entity:move(x,y)
	self.goalx = x or self.goalx
	self.goaly = y or self.goaly
end

function Entity:addComponent(name)
	table.insert(self.components, name)
end

function Entity:setFilter(filter)
	self.filter = filter
end

return Entity