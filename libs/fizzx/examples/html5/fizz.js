var statics = [];
var dynamics = [];
var gravity = 0;
var maxVelocity = 200;

// returns shape index and its list
function findShape(s)
{
	var t = s.list;
	for (k = 0; k < t.length; k++) {
		if (t[k] == s) return k;
	}
}

// removes shape from its list
function removeShape(s)
{
	var k, t = findShape(s)
	if (k) {
		s.list = null;
		t.splice(k, 1);
	}
}

// rects have a center position and half-width/height
function addRectShape(list, x, y, w, h)
{
	var s = { list:list, x:x, y:y, hw:w/2, hh:h/2, xv:0, yv:0 }
	s.bounce = 0;
	s.friction = 0;
	list.push(s);
	return s;
}

// static shapes do not move or respond to collisions
function addStaticRect(x, y, w, h)
{
	return addRectShape(statics, x, y, w, h);
}

// dynamic shapes are affected by gravity and collisions
function addDynamicRect(x, y, w, h)
{
	var s = addRectShape(dynamics, x, y, w, h);
	s.damping = 0;
	return s;
}

function testRectRect(a, b, sep)
{
	// distance between the shapes
	var dx = a.x - b.x;
	var dy = a.y - b.y;
	var adx = Math.abs(dx);
	var ady = Math.abs(dy);
	// sum of the extents
	var shw = a.hw + b.hw;
	var shh = a.hh + b.hh;
	if (adx >= shw || ady >= shh) {
		// no intersection
		return false;
	}
	// intersection
	// shortest separation
	var sx = shw - adx;
	var sy = shh - ady;
	// ignore the longer axis
	if (sx < sy) {
		if (sx > 0) sy = 0;
	}
	else {
		if (sy > 0) sx = 0;
	}
	// correct sign
	if (dx < 0) sx = -sx;
	if (dy < 0) sy = -sy;
	sep.x = sx;
	sep.y = sy;
	return true;
}

// moves shape by given amount without checking for collisions
function moveShape(a, dx, dy)
{
	a.x = a.x + dx;
	a.y = a.y + dy;
}

// updates the simulation
function update(dt)
{
	// update velocity vectors
	var g = gravity*dt;
	var maxVelocity2 = maxVelocity*maxVelocity;
	for (i = 0; i < dynamics.length; i++) {
		var d = dynamics[i];
		var xv = d.xv;
		var yv = d.yv;
		// damping
		var k = 1 + d.damping*dt;
		xv = xv/k;
		yv = yv/k;
		// gravity
		yv = yv + g;
		// clamp
		var v2 = xv*xv + yv*yv;
		if (v2 > maxVelocity2) {
			var n = 1/Math.sqrt(v2)*maxVelocity;
			xv = xv*n;
			yv = yv*n;
		}
		// update
		d.xv = xv;
		d.yv = yv;
	}
	// move dynamic objects
	for (i = 0; i < dynamics.length; i++) {
		var d = dynamics[i];
		moveShape(d, d.xv*dt, d.yv*dt);
		// check and resolve collisions
		for (j = 0; j < statics.length; j++) {
			checkCollision(d, statics[j]);
		}
		for (j = i + 1; j < dynamics.length; j++) {
			checkCollision(d, dynamics[j]);
		}
	}
}

// checks for collisions
var sep = [];
function checkCollision(a, b)
{
	var c = testRectRect(a, b, sep);
	if (c == true) {
		solveCollision(a, b, sep.x, sep.y);
	}
}

// resolves collision
function solveCollision(a, b, sx, sy)
{
	// find the collision normal
	var d = Math.sqrt(sx*sx + sy*sy);
	var nx = sx/d;
	var ny = sy/d;
	// relative velocity
	var vx = a.xv - b.xv;
	var vy = a.yv - b.yv;
	// penetration speed
	var ps = vx*nx + vy*ny;
	// objects moving apart?
	if (ps > 0) {
		return;
	}
	// penetration component
	var px = nx*ps;
	var py = ny*ps;
	// tangent component
	var tx = vx - px;
	var ty = vy - py;
	// restitution
	var r = 1 + Math.max(a.bounce, b.bounce);
	// friction
	var f = Math.min(a.friction, b.friction);
	// adjust the velocity of shape a
	a.xv = a.xv - px*r + tx*f;
	a.yv = a.yv - py*r + ty*f;
	if (b.list == dynamics) {
		// adjust the velocity of shape b
		b.xv = b.xv + (px*r + tx*f);
		b.yv = b.yv + (py*r + ty*f);
	}
	// separate the two shapes
	moveShape(a, sx, sy);
}