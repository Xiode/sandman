var width = 24*16;
var height = 18*16;
var tfps = 60;

var canvas = document.getElementById("canvas");
canvas.width = width;
canvas.height = height;
var context = canvas.getContext("2d");

// input
var keys = [];
document.body.addEventListener("keydown", function (e) {
	keys[e.keyCode] = true;
});
document.body.addEventListener("keyup", function (e) {
	keys[e.keyCode] = false;
});

// map
var map = [
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1,
	1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1,
	1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1,
	1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1,
	1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1,
	1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
	1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1,
	1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1,
	1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
];
for (i = 0; i < map.length; i++) {
	if (map[i] == 1) {
		var x = i%24;
		var y = (i - x)/24;
		addStaticRect(x*16 + 8, y*16 + 8, 16, 16);
	}
}

// logic
var player = addDynamicRect(100, 100, 8, 8);
player.damping = 5;

function movement(dt) {
	if (keys[38]) {
		player.yv -= 25;
	}
	if (keys[40]) {
		player.yv += 25;
	}
	if (keys[37]) {
		player.xv -= 25;
	}
	if (keys[39]) {
		player.xv += 25;
	}
}

var accum = 0;
var interval = 1/tfps;
function step(dt) {
	accum = accum + dt;
	while (accum >= interval) {
		movement(interval);
		update(interval);
		accum = accum - interval;
	}
}

// rendering
function drawRect(r) {
	var hw = r.hw;
	var hh = r.hh;
	context.fillRect(r.x - hw, r.y - hh, hw*2, hh*2);
}

function draw(dt) {
	context.clearRect(0, 0, width, height);
	context.fillStyle = "red";
	for (i = 0; i < dynamics.length; i++)
	{
		drawRect(dynamics[i]);
	}
	context.fillStyle = "black";
	for (i = 0; i < statics.length; i++)
	{
		drawRect(statics[i]);
	}
	if (dt > 0) context.fillText('FPS:' + Math.ceil(1/dt), 30, 30);
}

// shim layer with setTimeout fallback
window.requestAnimFrame = window.requestAnimationFrame || 
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	function (callback) {
		window.setTimeout(callback, 1000/tfps);
	};

var last = new Date().getTime(); // Date.now();
function loop() {
	var current = new Date().getTime(); // Date.now();
	var dt = current - last;
	last = current;
	// delta in seconds
	dt = dt/1000
	// don't skip more than N frames
	// dt = Math.min(dt, 5/tfps);
	step(dt);
	requestAnimFrame(loop);
	draw(dt);
};
loop();