fizz = require("fizzx.fizz")
terminal = require("utils.log.terminal")

local WHITE = Color(255, 255, 255)

--fizz.partition = 'quad'
--fizz.setPartition('quad')

-- jump heights
local maxjumpH = 16*4
local minjumpH = 16*1
-- jump time to apex
local maxjumpT = 0.4
-- gravity
local g = (2*maxjumpH)/(maxjumpT^2)
-- initial jump velocity
local initjumpV = math.sqrt(2*g*maxjumpH)
-- jump termination velocity
local termjumpV = math.sqrt(initjumpV^2 + 2*-g*(maxjumpH - minjumpH))
-- jump termination time
local termjumpT = maxjumpT - (2*(maxjumpH - minjumpH)/(initjumpV + termjumpV))

-- default jump termination
local jumpTerm = termjumpV

-- tile size
local tile = 16
local tile2 = tile/2

fizz.setGravity(0, -g)
--quad.mincellsize = 32

map =
{
  1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 
  1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 
  1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 2, 2, 2, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 
  1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 
  1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 2, 2, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 
  1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 
  1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 2, 2, 2, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 
  1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
  1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
  1, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1, 
  1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 4, 1, 1, 
  1, 2, 2, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 0, 0, 0, 0, 0, 0, 4, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 4, 1, 1, 1, 
  1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 5, 5, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 4, 1, 1, 1, 1, 1, 0, 0, 0, 5, 0, 0, 0, 5, 0, 4, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1, 1, 1, 1, 
  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
}

local shapes =
{
  -- solid block
  [1] = { 'static', 'rect', 0, 0, tile2, tile2 },
  -- one-sided platform: _
  [2] = { 'static', 'line', -tile2, -tile2, tile2, -tile2 },
  -- right slope: \
  [3] = { 'static', 'line', -tile2, tile2, tile2, -tile2 },
  -- left slope: /
  [4] = { 'static', 'line', -tile2, -tile2, tile2, tile2 },
  -- dynamic circle: O
  [5] = { 'dynamic', 'circle', 0, 0, tile2 }
}

h = 16
w = #map/h

display:create("Fizz", w*tile, h*tile, 32, true)

-- todo todo todo: gets GCed
scene = Layer()
scene:add_child(display.viewport.camera)

local function drawShape(canvas, s)
  canvas:move_to(0, 0)
  if s.shape == "rect" then
    canvas:rectangle(s.hw*2, s.hh*2)
    canvas:fill()
  elseif s.shape == "circle" then
    canvas:circle(s.r)
    canvas:fill()
  elseif s.shape == "line" then
    canvas:line_to(s.x2 - s.x, s.y2 - s.y)
    canvas:stroke()
  end
end

local function createSprite(s)
  local sprite = Sprite(s.x, s.y)
  scene:add_child(sprite)
  drawShape(sprite.canvas, s)
  return sprite
end

-- create shapes and tile sprites
for x = 0, w - 1 do
  for y = 0, h - 1 do
    local i = map[y*w + x + 1]
    local wx, wy = x*tile - w*tile/2 + tile2, -y*tile + h*tile/2 - tile2
    local s2 = shapes[i]
    if s2 then
      -- copy
      local t = { unpack(s2) }
      local s = table.remove(t, 1)
      -- transform
      t[2] = t[2] + wx
      t[3] = t[3] + wy
      if t[1] == "line" then
        t[4] = t[4] + wx
        t[5] = t[5] + wy
      end
      -- create
      local shape
      if s == 'dynamic' then
        shape = fizz.addDynamic(unpack(t))
      else
        shape = fizz.addStatic(unpack(t))
      end
      shape.friction = 1
      shape.sprite = createSprite(shape)
    end
  end
end

-- player
p = fizz.addDynamic('rect', 3*tile, 10*tile, tile2/2, tile2/2)
--p = fizz.addDynamic('circle', 3*tile, 10*tile, tile2/2)
p.friction = 0.25
-- player flags
p.grounded = false
p.jumping = false
p.moving = false

p.sprite = createSprite(p)

-- callback for player collisions
function p:onCollide(b, nx, ny, pen)
  -- return false if you want to ignore the collision
  return true
end

-- process user input
function p:checkInput(dt)
  -- get user input
  local left = keyboard:is_down(87)
  local right = keyboard:is_down(89)
  local jump = keyboard:is_down(72)

  -- get player velocity
  local vx, vy = fizz.getVelocity(p)
  -- get the player displacement
  local sx, sy = fizz.getDisplacement(p)

  -- something is pushing the player up?
  p.grounded = false
  if sy > 0 then
    p.grounded = true
    p.jumping = false
  end

  -- running (horizontal movement)
  p.moving = left or right
  if p.moving then
    -- movement vector
    local move = 1000
    if left then
      move = -1000
    end
    -- slower movement while in the air
    if not p.grounded then
      move = move/8
    end
    -- add to player velocity
    vx = vx + move*dt
  end

  -- jumping (vertical movement)
  if jump and not p.jumping and p.grounded then
    -- initiating a jump
    p.jumping = true
    vy = initjumpV
  elseif not jump and p.jumping and not p.grounded then
    -- terminating a jump
    if p.yv > 0 and p.yv > jumpTerm then
      vy = jumpTerm
    end
    p.jumping = false
  end
  
  -- update player velocity
  fizz.setVelocity(p, vx, vy)
end

-- kinematic platform
k = fizz.addKinematic('rect', 10*tile, 10*tile, 30, 10)
k.yv = 50
k.sprite = createSprite(k)

function redraw()
  -- sync sprite to shape
  for i, v in ipairs(fizz.kinematics) do
    v.sprite.x = v.x
    v.sprite.y = v.y
  end
  for i, v in ipairs(fizz.dynamics) do
    v.sprite.x = v.x
    v.sprite.y = v.y
  end
end

-- update interval in seconds
interval = 1/60
-- maximum frame skip
maxsteps = 5
-- accumulator
accum = 0

timer = Timer()
timer:start(16, true)
function timer:on_tick()
  local steps = 0
  -- update the simulation
  local dt = timer:get_delta_ms()/1000
  accum = accum + dt
  while accum >= interval do
    -- handle player input
    p:checkInput(interval)
    
    -- update the simulation
    fizz.update(interval)

    accum = accum - interval
    steps = steps + 1
    if steps >= maxsteps then
      break
    end
  end

  redraw()
  
  terminal.trace("checks", fizz.nchecks)
  
  --[[
  terminal.printf("livecells: %d", quad.livecells)
  terminal.printf("deadcells: %d", #quad.deadcells)
  terminal.printf("objects: %d", quad.objects)
  terminal.printf("rootsize: %d", quad.root.s)
  ]]
  terminal.trace("steps", steps)
  terminal.trace("part", fizz.partition)

  terminal.trace("grounded", tostring(p.grounded))
  terminal.trace("jumping", tostring(p.jumping))
  terminal.trace("moving", tostring(p.moving))
  terminal.trace("separation", p.sx .. " " .. p.sy)
  terminal.trace("velocity", p.xv .. " " .. p.yv)
  terminal.trace("partition", fizz.getPartition())
  terminal.trace("cchecks", fizz.getCollisionCount())

  terminal.trace("gravity", g)
  local jt = "gradual"
  if jumpTerm == 0 then
    jt = "instant"
  end
  terminal.trace("jump termination", jt)
end

function keyboard:on_press(k)
  if k == KEY_P then
    local p = fizz.getPartition()
    if p == "quad" then
      p = "none"
    else
      p = "quad"
    end
    fizz.setPartition(p)
  elseif k == KEY_J then
    if jumpTerm == 0 then
      jumpTerm = termjumpV
    else
      jumpTerm = 0
    end
  end
end