fizz = require("fizzx.fizz")
terminal = require("utils.log.terminal")

local WHITE = Color(255, 255, 255)
local YELLOW = Color(255, 0, 255)

fizz.setPartition(true)

fizz.setGravity(0, -1000)

display:create("Fizz", 800, 600, 32, true)

sprite = Sprite()
canvas = sprite.canvas
display.viewport:add_child(sprite)

profile = require('utils.log.profile')
profile.hookall()
--profile.start()

local function drawShape(canvas, s)
  canvas:set_line_style(2, WHITE)
  canvas:move_to(s.x, s.y)
  if s.shape == "rect" then
    canvas:rectangle(s.hw*2, s.hh*2)
    canvas:fill()
  elseif s.shape == "circle" then
    canvas:circle(s.r)
    canvas:fill()
  elseif s.shape == "line" then
    canvas:line_to(s.x2, s.y2)
    canvas:stroke()
  end
  if s.xv and s.yv then
    canvas:set_line_style(2, YELLOW)
    canvas:rel_line_to(s.xv, s.yv)
    canvas:stroke()
  end
  canvas:move_to(s.x, s.y)
end

function redraw(canvas)
  canvas:clear()
  for i, s in ipairs(fizz.dynamic) do
    drawShape(canvas, s)
  end
  for i, s in ipairs(fizz.static) do
    drawShape(canvas, s)
  end
end

--[[
-- callback for player collisions
function p:onCollide(b, nx, ny, pen)
  -- return false if you want to ignore the collision
  return true
end
]]

fizz.static = {}

fizz.static[1] = fizz.addStatic("rect", 400, 0, 50, 600)
fizz.static[2] = fizz.addStatic("rect", -400, 0, 50, 600)
fizz.static[3] = fizz.addStatic("rect", 0, 300, 800, 50)
fizz.static[4] = fizz.addStatic("rect", 0, -300, 800, 50)
for i, v in pairs(fizz.static) do
  fizz.static[i].bounce = 0
end

fizz.dynamic = {}

for i = 1, 51 do
  local x, y = math.random(-300, 300), math.random(-200, 200)
  local r = 1--math.random(2)
  if r == 1 then
    local radius = math.random(10, 40)
    fizz.dynamic[i] = fizz.addDynamic("circle", x, y, radius)
    fizz.setDensity(fizz.dynamic[i], 10)
    --fizz.dynamic[i].mass = radius
  else
    local side = math.random(5, 20)
    fizz.dynamic[i] = fizz.addDynamic("rect", x, y, side, side)
  end
  --fizz.dynamic[i].xv = math.random(-20, 20)
  --fizz.dynamic[i].yv = math.random(-20, 20)
  fizz.dynamic[i].friction = 1
  fizz.dynamic[i].bounce = 0
end

-- update interval in seconds
interval = 1/60
-- maximum frame skip
maxsteps = 5
-- accumulator
accum = 0
-- frame
frame = 0

timer = Timer()
timer:start(16, true)
function timer:on_tick()
  local steps = 0
  -- update the simulation
  halt = false
  if not halt then
    local dt = timer:get_delta_ms()/1000
    accum = accum + dt
  end
  while accum >= interval and not halt do
    if mouse:is_down(108) and selection then
      selection.xv = (mouse.xaxis - selection.x)*5
      selection.yv = (mouse.yaxis - selection.y)*5
      fizz.repartition(selection)
    end
    -- update the simulation
    fizz.update(interval)

    accum = accum - interval
    steps = steps + 1
    if steps >= maxsteps then
      break
    end
  end

  redraw(canvas)
  frame = frame + 1
  
  if frame%10 == 0 then
    terminal.clear()
    terminal.print(profile.report('time', 20))
    profile.reset()
  end

  terminal.trace("checks", fizz.nchecks)

  terminal.trace("steps", steps)
  terminal.trace("partition", fizz.getPartition())
  local cc, mc = fizz.getCollisionCount()
  terminal.trace("cchecks", cc .. '/' .. mc)
  terminal.trace("frame", frame)
  
end

selection = nil
function mouse:on_press()
  halt = false
  for i, s in ipairs(fizz.dynamic) do
    local dx, dy = s.x - mouse.xaxis, s.y - mouse.yaxis
    local d = math.sqrt(dx*dx + dy*dy)
    if d < (s.r or s.hw) then
      selection = s
      return
    end
  end
  selection = nil
end
